<?php
/**
 * Fonctions utiles au plugin Publications HAL
 *
 * @plugin     Publications HAL
 * @copyright  2016-2023
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\hal_m\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Extrait les données du champs  authFullNamePersonIDIDHal_fs
 * Auteur : nom complet (par defaut si auteur identifié) + _FacetSep_ + personID + _FacetSep_ + IdHal (pour la facette de recherche auteur)
 * Joseph Larmarange_FacetSep_5290_FacetSep_joseph-larmarange -> Joseph Larmarange, 5290,  joseph-larmarange
 *
 * @param str $string
 * @return array
 */
function hal_m_extraire_authFullNamePersonIDIDHal_fs($string) {
	$r = [];
	$motif = '_FacetSep_';

	if ($nom = strstr($string, $motif, true)) {
		$r['nom'] = $nom;
		$reste = substr($string, strlen($nom.$motif));
		if ($id = strstr($reste, $motif, true)) {
			$r['id'] = $id;
			$reste = substr($reste, strlen($id.$motif));
			if ($reste) {
				$r['hal'] = $reste;
			}
		}
	};

	return $r;
}

