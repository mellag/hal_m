# Plugin HAL

![HAL](./prive/themes/spip/images/hal_m-xx.svg)

Le plugin HAL permet d'afficher les publications du portail [HAL](https://hal.science/) à travers un modèle SPIP unique ```<hal|param1|param2|...>```


## Le modèle ```<hal>```

Tous les paramètres sont facultatifs.  Si plusieurs paramètres sont indiqués, seules les références remplissant TOUTES les conditions seront sélectionnées.


| Paramètre |  Explications  | Exemple |
| -------- | -------- |-------- |
| q    |  requête de recherche  sur l'API HAL https://api.archives-ouvertes.fr/docs/search   | `<hal\|q=Mali>` |
| collection     | restreindre à une collection     |`<hal\|collection=CEPED>`|
| auteur     | restreindre à un auteur - fournir `idHal`     |`<hal\|auteur=joseph-larmarange>`|
| max    | nombre de résultats affiché de 0 à 10000. Valeur par défaut défini dans panel de configuration du plugin sinon 10     |`<hal\|max=4>`|
| max_auteurs | nombre d'auteurs affiché. Valeur par défaut défini dans panel de configuration du plugin sinon 3 . et al. est affiché si la liste est retreinte.   |`<hal\|max_auteurs=3>`|
| tri     | indique le tri de requete. par défaut pertinence											https://api.archives-ouvertes.fr/docs/search/?#sort    | `<hal\|tri=submittedDate_tdate desc>`|
| debug     | affiche la requête vers l'API HAL pour aider au déboguage    |`<hal\|debug=oui>`|
| dernieres_publications     | via class, si renseigné, on force le tri sur le critère `publicationDate_tdate`    |`<hal\|dernieres_publications>`|
| masquer_lien    | valable uniquement si `dernieres_publications` est renseignée. si renseigné, on n'affiche pas les liens *voir toutes publications* après la liste des résultats   |`<hal\|dernieres_publications\|auteur=joseph-larmarange\|masquer_lien=oui>` |
| halid   | retourne le document unique correspondant au `HALid`   |`<hal\|halid=ird-03950125>`|
| doi	   | retourne le document unique correspondant au `doi` |`<hal\|doi=10.1371/journal.pone.0280479>`|



## Documentation de l'API HAL
https://api.archives-ouvertes.fr/docs/search

## Documentation du plugin SPIP
https://contrib.spip.net/5478
