<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bibliographie_hal' => 'Bibliographie HAL',
	'bibliographie_hal_reference' => 'Bibliographie HAL: référence unique',
	'bibliographie_hal_dernieres_publications' => 'Bibliographie HAL: dernières publications',

	// C
	'cfg_titre_parametrages' => 'Configuration HAL',

	// E
	'explication_max' => 'Nombre maximum de publications affichées par défaut si le critère <code>max</code> n\'est pas renseigné',
	'explication_max_auteurs' => 'Nombre maximum d\'auteurs affichés par défaut si le critère <code>max_auteurs</code> n\'est pas renseigné',


	// H
	'hal_m_titre' => 'HAL modèles',
	'hal_more_auteur' => 'Voir toutes les publications de cet auteur·e',
	'hal_more_collection' => 'Voir la liste complète des publications',
	'hal_debug_requete' => 'Requête générée:',

	// L
	'label_champs' => 'Champs à retourner',
	'label_max' => 'Nombre maximum de références à lister',
	'label_max_auteurs' => 'Nombre maximum d\'auteurs à lister',
	'label_max_explication' => 'Optionnel, si non spécifié, la valeur par défaut définie dans la configuration du plugin sera utilisée',
	'label_max_auteurs_explication' => 'Optionnel, si non spécifié, la valeur par défaut définie dans la configuration du plugin sera utilisée',
	'label_halid' => 'Référence spécifique (HALid)',
	'label_halid_explication' => 'Identifiant de la référence (par exemple : <em>hal-12345678</em>)',
	'label_doi' => 'Référence DOI',
	'label_doi_explication' => 'DOI de la référence (par exemple : <em>10.1234/journal.45.6789</em>)',
	'label_parametres' => 'Paramètres de sélection',
	'label_parametres_techniques' => 'Paramètres techniques',
	'label_parametres_explication' => 'Indiquez le ou les filtres de sélection. Si plusieurs filtres sont indiqués, seules les références remplissant TOUTES les conditions seront sélectionnées.',
	'label_q' => 'Requête générique (q)',
	'label_q_explication' => 'Voir <a href="https://api.archives-ouvertes.fr/docs/search" target="_blank">l\'API HAL</a>',
	'label_collection' => 'Collection',
	'label_collection_explication' => 'Indiquez le nom de la collection tel qu\'il apparait dans l\'URL du portail de la collection',
	'label_auteure' => 'Auteur·e',
	'label_auteure_explication' => 'à partir de son idHAL uniquement (pour une recherche à partir du nom, utiliser une requête générique)',
	'label_tri' => 'Tri',
	'label_tri_explication' => 'Ignoré si l\'option <em>Dernières publications</em> est sélectionnée, voir la documentation de l\'<a href="https://api.archives-ouvertes.fr/docs/search/?#sort" target="_blank">API HAL</a> pour les valeurs possibles.',
	'label_debug' => 'Mode débugage',
	'label_debug_explication' => 'Afficher la requête générée',
	'label_variante' => 'Variante',
	'label_dernieres_pubs' => 'Dernières publications ?',
	'label_dernieres_pubs_explication' => 'Sélectionner les dernières publications, triées par date décroissante et ajouter un lien vers le CV de l\'auteur et/ou le portail de la collection',
	'label_masquer_lien' => 'Masquer les liens supplémentaires',
	'label_masquer_lien_case_explication' => 'Ne pas afficher les liens vers le CV de l\'auteur et/ou le portail de la collection',




	// T
	'titre_page_configurer_hal_pub' => 'Paramètres HAL',
	// les types de documents
	// https://api.archives-ouvertes.fr/ref/doctype
	'type_pub_ART' => 'Article dans une revue',
	'type_pub_COMM' => 'Communication dans un congrès',
	'type_pub_POSTER' => 'Poster de conférence',
	'type_pub_PROCEEDINGS' => 'Proceedings/Recueil des communications',
	'type_pub_ISSUE' => 'N°spécial de revue/special issue',
	'type_pub_OUV' => 'Ouvrage',
	'type_pub_COUV' => 'Chapitre d\'ouvrage',
	'type_pub_BLOG' => 'Article de blog scientifique',
	'type_pub_NOTICE' => 'Notice d\'encyclopédie ou de dictionnaire',
	'type_pub_TRAD' => 'Traduction',
	'type_pub_PATENT' => 'Brevet',
	'type_pub_OTHER' => 'Autre publication scientifique',
	'type_pub_UNDEFINED' => 'Pré-publication, Document de travail',
	'type_pub_REPORT' => 'Rapport',
	'type_pub_THESE' => 'Thèse',
	'type_pub_HDR' => 'HDR',
	'type_pub_LECTURE' => 'Cours',
	'type_pub_MEM' => 'Mémoire d\'étudiant',
	'type_pub_IMG' => 'Image',
	'type_pub_VIDEO' => 'Vidéo',
	'type_pub_SON' => 'Son',
	'type_pub_MAP' => 'Carte',
	'type_pub_SOFTWARE' => 'Logiciel',
	'type_pub_PRESCONF' => 'Document associé à des manifestations scientifiques',
	'type_pub_CREPORT' => 'Chapitre de rapport',
	'type_pub_ETABTHESE' => 'Thèse d\'établissement',
	'type_pub_MEMLIC' => 'MEMLIC',
	'type_pub_NOTE' => 'Note de lecture',
	'type_pub_OTHERREPORT' => 'Autre rapport, séminaire, workshop',
	'type_pub_REPACT' => 'Rapport d\'activité',
	'type_pub_SYNTHESE' => 'Note de synthèse',
	'type_pub_POSTER' => 'Poster de conférence',
	'type_pub_PROCEEDINGS' => 'Proceedings/Recueil des communications',
	'type_pub_ISSUE' => 'N°spécial de revue/special issue',
	'type_pub_BLOG' => 'Article de blog scientifique',
	'type_pub_NOTICE' => 'Notice d\'encyclopédie ou de dictionnaire',
	'type_pub_TRAD' => 'Traduction',
	'type_pub_ARTREV' => 'Article de synthèse',
	'type_pub_DATAPAPER' => 'Data paper',
	'type_pub_BOOKREVIEW' => 'Compte-rendu de lecture',
	'type_pub_CRIT' => 'Edition critique',
	'type_pub_MANUAL' => 'Manuel',
	'type_pub_SYNTOUV' => 'Ouvrage de synthèse',
	'type_pub_DICTIONARY' => 'Dictionnaire, encyclopédie',
	'type_pub_PREPRINT' => 'Preprint/prepublication',
	'type_pub_WORKINGPAPER' => 'Working paper',
	'type_pub_RESREPORT' => 'Rapport de recherche',
	'type_pub_TECHREPORT' => 'Rapport technique',
	'type_pub_FUNDREPORT' => 'Rapport contrat/projet',
	'type_pub_EXPERTREPORT' => 'Rapport d\'expertise collective',
	'type_pub_DMP' => 'Plan de gestion de données/data management plan',
	'type_pub_PHOTOGRAPHY' => 'Photographie',
	'type_pub_DRAWING' => 'Dessin',
	'type_pub_ILLUSTRATION' => 'Illustration',
	'type_pub_GRAVURE' => 'Gravure',
	'type_pub_GRAPHICS' => 'Image de synthèse',

);

