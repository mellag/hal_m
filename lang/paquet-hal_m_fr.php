<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = [

	// H
	'hal_m_description' => 'Affiche les ressources bibliographiques HAL',
	'hal_m_nom' => 'HAL',
	'hal_m_slogan' => 'Affiche les publications HAL à travers un modèle SPIP',
];
